// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectFruitAction.h"

#include "GridNode.h"
#include "LevelGenerator.h"
#include "Ship.h"

CollectFruitAction::CollectFruitAction()
{
	Reset();
}

CollectFruitAction::~CollectFruitAction()
{
}

bool CollectFruitAction::IsActionDone()
{
	if (FruitGathered >= FruitToGather)
	{
		return true;
	}

	return false;
}

bool CollectFruitAction::CheckProceduralPreconditions(AShip* Ship)
{
	if (!Ship)
	{
		return false;
	}
	GridNode* GoalNode = Ship->Level->CalculateNearestGoal(Ship->xPos, Ship->yPos, GridNode::FruitResource);

	if (!GoalNode || !GoalNode->ResourceAtLocation)
	{
		return false;
	}

	Target = GoalNode->ResourceAtLocation;

	FVector Dist = Ship->GetActorLocation() - Target->GetActorLocation();

	if (Dist.Size() <= 5)
	{
		SetInRange(true);
	}
	else
	{
		SetInRange(false);
	}
	
	return true;
}

bool CollectFruitAction::PerformAction(AShip* Ship, float DeltaTime)
{
	ActionTime += DeltaTime;
	AFruit* FruitResource = Cast<AFruit>(Target);

	if (!FruitResource || !Ship->Level->IsFruitValid(FruitResource))
		return false;

	if (ActionTime >= TimeToCollect)
	{
		FruitGathered++;
		// add one Fruit
		Ship->NumFruit += 1;
		// decrease one morale each action
		Ship->morale -= 1;
		Ship->Level->CollectFruit(FruitResource);
		Target = nullptr;
	}

	

	return true;
}

bool CollectFruitAction::RequiresInRange()
{
	return true;
}

void CollectFruitAction::Reset()
{
	SetInRange(false);
	Target = nullptr;
	FruitGathered = 0;
	ActionTime = 0;
}
