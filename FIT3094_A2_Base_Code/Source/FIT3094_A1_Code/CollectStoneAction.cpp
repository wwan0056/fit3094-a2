// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectStoneAction.h"

#include "LevelGenerator.h"
#include "Ship.h"
#include "Stone.h"

CollectStoneAction::CollectStoneAction()
{
	Reset();
}

CollectStoneAction::~CollectStoneAction()
{
}

bool CollectStoneAction::IsActionDone()
{
	if (StoneGathered >= StoneToGather)
	{
		return true;
	}
	return false;
}

bool CollectStoneAction::CheckProceduralPreconditions(AShip* Ship)
{
	if (!Ship)
	{
		return false;
	}

	GridNode* GoalNode = Ship->Level->CalculateNearestGoal(Ship->xPos, Ship->yPos, GridNode::StoneResource);

	if (!GoalNode || !GoalNode->ResourceAtLocation)
	{
		return false;
	}

	Target = GoalNode->ResourceAtLocation;

	FVector Dist = Ship->GetActorLocation() - Target->GetActorLocation();

	if (Dist.Size() <= 5)
	{
		SetInRange(true);
	}
	else
	{
		SetInRange(false);
	}

	return true;
}

bool CollectStoneAction::PerformAction(AShip* Ship, float DeltaTime)
{
	ActionTime += DeltaTime;
	AStone* StoneResource = Cast<AStone>(Target);

	if (!StoneResource || !Ship->Level->IsStoneValid(StoneResource))
		return false;

	if (ActionTime >= TimeToCollect)
	{
		StoneGathered++;
		// add one Stone
		Ship->NumStone += 1;
		// decrease one morale each action
		Ship->morale -= 1;
		Ship->Level->CollectStone(StoneResource);
		Target = nullptr;
	}

	return true;
}

bool CollectStoneAction::RequiresInRange()
{
	return true;

}

void CollectStoneAction::Reset()
{
	SetInRange(false);
	Target = nullptr;
	StoneGathered = 0;
	ActionTime = 0;
}
