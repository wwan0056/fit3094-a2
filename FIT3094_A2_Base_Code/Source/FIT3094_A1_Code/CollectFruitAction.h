// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GOAPAction.h"
#include "ResourceActor.h"

/**
 * 
 */
class FIT3094_A1_CODE_API CollectFruitAction: public GOAPAction
{
public:
	CollectFruitAction();
	~CollectFruitAction();

	bool IsActionDone() override;
	virtual bool CheckProceduralPreconditions(AShip* Ship) override;
	virtual bool PerformAction(AShip* Ship, float DeltaTime) override;
	virtual bool RequiresInRange() override;
	
private:
	virtual void Reset();

	// How many treasure needs to be gathered for the action to completed
	const int FruitToGather = 1;
	// How long does it take to gather a treasure. Actions are not instant they can take time!
	const int TimeToCollect =1;
	// How many treasure have been gathered so far. This will either be 0 or 1 all times
	int FruitGathered;
	// How much time has elapsed inside this action so far. This is used to determine if the action completes
	float ActionTime;

	AResourceActor* ResourceNode;
	
};
