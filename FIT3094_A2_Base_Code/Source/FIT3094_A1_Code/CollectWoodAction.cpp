// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectWoodAction.h"

#include "GridNode.h"
#include "LevelGenerator.h"
#include "Ship.h"

CollectWoodAction::CollectWoodAction()
{
	Reset();
}

CollectWoodAction::~CollectWoodAction()
{
}


bool CollectWoodAction::IsActionDone()
{
	if (WoodGathered >= WoodToGather)
	{
		return true;
	}

	return false;
}

bool CollectWoodAction::CheckProceduralPreconditions(AShip* Ship)
{
	if (!Ship)
	{
		return false;
	}

	GridNode* GoalNode = Ship->Level->CalculateNearestGoal(Ship->xPos, Ship->yPos, GridNode::WoodResource);

	if (!GoalNode || !GoalNode->ResourceAtLocation)
	{
		return false;
	}

	Target = GoalNode->ResourceAtLocation;

	FVector Dist = Ship->GetActorLocation() - Target->GetActorLocation();

	if (Dist.Size() <= 5)
	{
		SetInRange(true);
	}
	else
	{
		SetInRange(false);
	}

	return true;
}

bool CollectWoodAction::PerformAction(AShip* Ship, float DeltaTime)
{
	ActionTime += DeltaTime;
	AWood* WoodResource = Cast<AWood>(Target);

	if (!WoodResource || !Ship->Level->IsWoodValid(WoodResource))
		return false;

	if (ActionTime >= TimeToCollect)
	{
		WoodGathered++;
		// add one wood
		Ship->NumWood += 1;
		// decrease one morale each action
		Ship->morale -= 1;
		Ship->Level->CollectWood(WoodResource);
		Target = nullptr;
	}

	return true;
}

bool CollectWoodAction::RequiresInRange()
{
	return true;
}

void CollectWoodAction::Reset()
{
	SetInRange(false);
	Target = nullptr;
	WoodGathered = 0;
	ActionTime = 0;
}