// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Ship.h"
#include "WoodShip.generated.h"

/**
 * 
 */
UCLASS()
class FIT3094_A1_CODE_API AWoodShip : public AShip
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	
private:
	const int MaxMorale = 100;
	const int MinMorale = 15;
	int Morale = 100;

public:
	AWoodShip();

	virtual void Tick(float DeltaSeconds) override;

	TMap<FString, bool> GetWorldState();
	TMap<FString, bool> GetGoalState();
	virtual void OnPlanFailed(TMap<FString, bool> FailedGoalState);
	virtual void OnPlanAborted(GOAPAction* FailedAction);

	void DecreaseMorale();
	void Heal() { Morale = MaxMorale; }

	int GetMorale() { return Morale; }
	int GetMinMorale() { return MinMorale; }

	FTimerHandle TimerHandle;

	UPROPERTY(EditAnywhere, Category = "Resources")
		int NumWoodResources;
};
