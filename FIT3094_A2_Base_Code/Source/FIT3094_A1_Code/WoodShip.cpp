// Fill out your copyright notice in the Description page of Project Settings.


#include "WoodShip.h"

#include "CollectTreasureAction.h"

AWoodShip::AWoodShip(): AShip()
{
	NumWoodResources = 0;
}

void AWoodShip::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(TimerHandle, this, &AWoodShip::DecreaseMorale, 2.0f, true, 2.0f);

	// Assign the actions to the actor
	/*CollectWoodAction* CollectWood = new CollectWoodAction();
	CollectWood->AddPrecondition("HasWoodResource", false);
	CollectWood->AddEffect("HasWoodResource", true);
	AvailableActions.Add(CollectWood);

	DropWoodAction* DropWood = new DropWoodAction();
	DropWood->AddPrecondition("DropWoodResource", false);
	DropWood->AddEffect("DropWoodResource", true);
	AvailableActions.Add(DropWood);*/

	CollectTreasureAction* TreasureAction = new CollectTreasureAction();
	TreasureAction->AddPrecondition("HasMorale", false);
	TreasureAction->AddEffect("HasMorale", true);
	AvailableActions.Add(TreasureAction);
}

void AWoodShip::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

TMap<FString, bool> AWoodShip::GetWorldState()
{
	TMap<FString, bool> WorldState = Super::GetWorldState();

	WorldState.Add("HasWoodResource", NumWoodResources == 50);
	WorldState.Add("DropWoodResource", NumWoodResources == 0);
	WorldState.Add("CollectTreasure", Morale > MinMorale);

	return WorldState;
}

TMap<FString, bool> AWoodShip::GetGoalState()
{
	TMap<FString, bool> GoalState;

	if (Morale < MinMorale)
	{
		GoalState.Add("CollectTreasure", true);
	}
	else
	{
		if (NumWoodResources != 50)
		{
			GoalState.Add("HasWoodResource", true);
		}
		else if (NumWoodResources != 0)
		{
			GoalState.Add("DropWoodResource", true);
		}
	}

	return GoalState;
}

void AWoodShip::DecreaseMorale()
{
	Morale--;

	if (Morale <= 0)
	{
		GetWorldTimerManager().ClearTimer(TimerHandle);
		Destroy();
	}
}

void AWoodShip::OnPlanFailed(TMap<FString, bool> FailedGoalState)
{
	Super::OnPlanFailed(FailedGoalState);
}

void AWoodShip::OnPlanAborted(GOAPAction* FailedAction)
{
	Super::OnPlanAborted(FailedAction);
}
