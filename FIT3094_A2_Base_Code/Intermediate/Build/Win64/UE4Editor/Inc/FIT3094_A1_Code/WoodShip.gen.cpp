// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FIT3094_A1_Code/WoodShip.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWoodShip() {}
// Cross Module References
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AWoodShip_NoRegister();
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AWoodShip();
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AShip();
	UPackage* Z_Construct_UPackage__Script_FIT3094_A1_Code();
// End Cross Module References
	void AWoodShip::StaticRegisterNativesAWoodShip()
	{
	}
	UClass* Z_Construct_UClass_AWoodShip_NoRegister()
	{
		return AWoodShip::StaticClass();
	}
	struct Z_Construct_UClass_AWoodShip_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumWoodResources_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NumWoodResources;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWoodShip_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AShip,
		(UObject* (*)())Z_Construct_UPackage__Script_FIT3094_A1_Code,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWoodShip_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "WoodShip.h" },
		{ "ModuleRelativePath", "WoodShip.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWoodShip_Statics::NewProp_NumWoodResources_MetaData[] = {
		{ "Category", "Resources" },
		{ "ModuleRelativePath", "WoodShip.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AWoodShip_Statics::NewProp_NumWoodResources = { "NumWoodResources", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWoodShip, NumWoodResources), METADATA_PARAMS(Z_Construct_UClass_AWoodShip_Statics::NewProp_NumWoodResources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWoodShip_Statics::NewProp_NumWoodResources_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWoodShip_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWoodShip_Statics::NewProp_NumWoodResources,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWoodShip_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWoodShip>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWoodShip_Statics::ClassParams = {
		&AWoodShip::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWoodShip_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWoodShip_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWoodShip_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWoodShip_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWoodShip()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWoodShip_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWoodShip, 1775313693);
	template<> FIT3094_A1_CODE_API UClass* StaticClass<AWoodShip>()
	{
		return AWoodShip::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWoodShip(Z_Construct_UClass_AWoodShip, &AWoodShip::StaticClass, TEXT("/Script/FIT3094_A1_Code"), TEXT("AWoodShip"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWoodShip);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
