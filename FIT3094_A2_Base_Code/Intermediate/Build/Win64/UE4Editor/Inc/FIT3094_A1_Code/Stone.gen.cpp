// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FIT3094_A1_Code/Stone.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStone() {}
// Cross Module References
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AStone_NoRegister();
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AStone();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FIT3094_A1_Code();
// End Cross Module References
	void AStone::StaticRegisterNativesAStone()
	{
	}
	UClass* Z_Construct_UClass_AStone_NoRegister()
	{
		return AStone::StaticClass();
	}
	struct Z_Construct_UClass_AStone_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AStone_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_FIT3094_A1_Code,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStone_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Stone.h" },
		{ "ModuleRelativePath", "Stone.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AStone_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AStone>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AStone_Statics::ClassParams = {
		&AStone::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AStone_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AStone_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AStone()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AStone_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AStone, 4216924089);
	template<> FIT3094_A1_CODE_API UClass* StaticClass<AStone>()
	{
		return AStone::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AStone(Z_Construct_UClass_AStone, &AStone::StaticClass, TEXT("/Script/FIT3094_A1_Code"), TEXT("AStone"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AStone);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
