// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT3094_A1_CODE_WoodShip_generated_h
#error "WoodShip.generated.h already included, missing '#pragma once' in WoodShip.h"
#endif
#define FIT3094_A1_CODE_WoodShip_generated_h

#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_SPARSE_DATA
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_RPC_WRAPPERS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWoodShip(); \
	friend struct Z_Construct_UClass_AWoodShip_Statics; \
public: \
	DECLARE_CLASS(AWoodShip, AShip, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AWoodShip)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAWoodShip(); \
	friend struct Z_Construct_UClass_AWoodShip_Statics; \
public: \
	DECLARE_CLASS(AWoodShip, AShip, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AWoodShip)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWoodShip(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWoodShip) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWoodShip); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWoodShip); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWoodShip(AWoodShip&&); \
	NO_API AWoodShip(const AWoodShip&); \
public:


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWoodShip(AWoodShip&&); \
	NO_API AWoodShip(const AWoodShip&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWoodShip); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWoodShip); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWoodShip)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_PRIVATE_PROPERTY_OFFSET
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_12_PROLOG
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_RPC_WRAPPERS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_INCLASS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_INCLASS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT3094_A1_CODE_API UClass* StaticClass<class AWoodShip>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_WoodShip_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
