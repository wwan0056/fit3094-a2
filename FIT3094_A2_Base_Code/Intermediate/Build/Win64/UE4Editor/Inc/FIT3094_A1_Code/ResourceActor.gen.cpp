// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FIT3094_A1_Code/ResourceActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeResourceActor() {}
// Cross Module References
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AResourceActor_NoRegister();
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AResourceActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FIT3094_A1_Code();
// End Cross Module References
	void AResourceActor::StaticRegisterNativesAResourceActor()
	{
	}
	UClass* Z_Construct_UClass_AResourceActor_NoRegister()
	{
		return AResourceActor::StaticClass();
	}
	struct Z_Construct_UClass_AResourceActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResourceCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ResourceCount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AResourceActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_FIT3094_A1_Code,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AResourceActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ResourceActor.h" },
		{ "ModuleRelativePath", "ResourceActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AResourceActor_Statics::NewProp_ResourceCount_MetaData[] = {
		{ "Category", "ResourceActor" },
		{ "ModuleRelativePath", "ResourceActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AResourceActor_Statics::NewProp_ResourceCount = { "ResourceCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AResourceActor, ResourceCount), METADATA_PARAMS(Z_Construct_UClass_AResourceActor_Statics::NewProp_ResourceCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AResourceActor_Statics::NewProp_ResourceCount_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AResourceActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AResourceActor_Statics::NewProp_ResourceCount,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AResourceActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AResourceActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AResourceActor_Statics::ClassParams = {
		&AResourceActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AResourceActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AResourceActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AResourceActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AResourceActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AResourceActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AResourceActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AResourceActor, 1590083997);
	template<> FIT3094_A1_CODE_API UClass* StaticClass<AResourceActor>()
	{
		return AResourceActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AResourceActor(Z_Construct_UClass_AResourceActor, &AResourceActor::StaticClass, TEXT("/Script/FIT3094_A1_Code"), TEXT("AResourceActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AResourceActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
