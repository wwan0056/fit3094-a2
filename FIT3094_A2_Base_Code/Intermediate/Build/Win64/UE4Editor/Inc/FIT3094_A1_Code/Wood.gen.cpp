// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FIT3094_A1_Code/Wood.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWood() {}
// Cross Module References
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AWood_NoRegister();
	FIT3094_A1_CODE_API UClass* Z_Construct_UClass_AWood();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FIT3094_A1_Code();
// End Cross Module References
	void AWood::StaticRegisterNativesAWood()
	{
	}
	UClass* Z_Construct_UClass_AWood_NoRegister()
	{
		return AWood::StaticClass();
	}
	struct Z_Construct_UClass_AWood_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWood_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_FIT3094_A1_Code,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWood_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Wood.h" },
		{ "ModuleRelativePath", "Wood.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWood_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWood>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWood_Statics::ClassParams = {
		&AWood::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWood_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWood_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWood()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWood_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWood, 3995627305);
	template<> FIT3094_A1_CODE_API UClass* StaticClass<AWood>()
	{
		return AWood::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWood(Z_Construct_UClass_AWood, &AWood::StaticClass, TEXT("/Script/FIT3094_A1_Code"), TEXT("AWood"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWood);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
