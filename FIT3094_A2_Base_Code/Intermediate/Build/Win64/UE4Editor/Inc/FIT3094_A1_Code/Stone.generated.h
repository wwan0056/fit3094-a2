// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT3094_A1_CODE_Stone_generated_h
#error "Stone.generated.h already included, missing '#pragma once' in Stone.h"
#endif
#define FIT3094_A1_CODE_Stone_generated_h

#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_SPARSE_DATA
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_RPC_WRAPPERS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStone(); \
	friend struct Z_Construct_UClass_AStone_Statics; \
public: \
	DECLARE_CLASS(AStone, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AStone)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAStone(); \
	friend struct Z_Construct_UClass_AStone_Statics; \
public: \
	DECLARE_CLASS(AStone, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FIT3094_A1_Code"), NO_API) \
	DECLARE_SERIALIZER(AStone)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStone(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStone) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStone); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStone(AStone&&); \
	NO_API AStone(const AStone&); \
public:


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStone(AStone&&); \
	NO_API AStone(const AStone&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStone); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStone)


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_PRIVATE_PROPERTY_OFFSET
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_9_PROLOG
#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_RPC_WRAPPERS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_INCLASS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_SPARSE_DATA \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_INCLASS_NO_PURE_DECLS \
	FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIT3094_A1_CODE_API UClass* StaticClass<class AStone>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT3094_A2_Base_Code_Source_FIT3094_A1_Code_Stone_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
